package com.example.hw17

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.util.*
import android.view.*

class MyView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr:Int = 0
): View(context, attrs, defStyleAttr), NewObject {
    private var iCircle:Int = 0
    private var iRectangle:Int = 0
    private var item: Shape? = null
    private var listOfObjects:ArrayList<Shape> = ArrayList()
    private var position = 0

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        for(i in listOfObjects){
            Log.d(i.type, "${i.x}, ${i.y}" )
            i.draw(canvas)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val x = event?.x
        val y = event?.y

        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                for(i in listOfObjects.indices){
                    if(check(x, y, listOfObjects[i])) {
                        item = listOfObjects[i]
                        position = i
                        break
                    }
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if(item!=null) {
                    moveObject(item!!, position, x!!, y!!)
                }
            }
            MotionEvent.ACTION_UP->{
                item = null
                position = 0
            }
        }
        return true
    }

    override fun addCircle() {
        val circle = Circle()
        circle.setColor()
        circle.id = "Circle$iCircle".hashCode()
        circle.type = "Circle"
        iCircle++
        listOfObjects.add(circle)
        invalidate()
    }

    override fun addRectangle() {
        val rectangle = Rectangle()
        rectangle.setColor()
        rectangle.id = "Rectangle$iRectangle".hashCode()
        rectangle.type = "Rectangle"
        iRectangle++
        listOfObjects.add(rectangle)
        invalidate()
    }

    private fun check(x:Float?, y:Float?, shape:Shape):Boolean{
        if(shape.type == "Circle"){
            val newX = x!!-(shape.x)
            val newY = y!!-(shape.y)
            val sqX = newX.times(newX)
            val sqY = newY.times(newY)
            if((sqX + sqY)<=shape.getRad().times(shape.getRad()))
                return true
            return false
        }
        if(shape.type == "Rectangle"){
            if(shape.x<=x!! && x <= shape.getWidth() && shape.y<=y!! && y <shape.getHeight()){
                return true
            }
        }
        return false
    }

    private fun moveObject(shape:Shape, i:Int, x:Float, y:Float){
        if(shape.type == "Circle"){
            shape.x = x
            shape.y = y
            listOfObjects[i] = shape
        }
        if(shape.type == "Rectangle"){
            shape.x = x - ((shape.getWidth()-shape.x)/2)
            shape.y = y - ((shape.getHeight()-shape.y)/2)
            shape.setWidth(x)
            shape.setHeight(y)
            listOfObjects[i] = shape
        }
        invalidate()
    }

    fun reset(){
        listOfObjects.clear()
        invalidate()
    }
}
