package com.example.hw17

import android.graphics.Canvas
import android.graphics.Paint

class Rectangle:Shape() {
    private val width = 500f
    private val height = 200f
    private var widthX = width + x
    private var heightY = height + y
    override fun draw(canvas: Canvas?){
        val paint: Paint = Paint()
        paint.apply {
            isAntiAlias = true
            color = colorOfShape
            style = Paint.Style.FILL
        }
        canvas?.drawRect(x, y, widthX, heightY, paint)
    }

    override fun setWidth(f:Float){
        widthX = x + width
    }

    override fun getWidth():Float{
        return widthX
    }

    override fun getHeight():Float{
        return heightY
    }

    override fun setHeight(f:Float){
        heightY = y + height
    }

}
