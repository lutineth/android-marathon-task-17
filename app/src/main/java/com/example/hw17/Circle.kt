package com.example.hw17

import android.graphics.Canvas
import android.graphics.Paint

class Circle: Shape() {
    private var radius:Float = 100f
    override var x = 200f
    override var y = 200f
    override fun draw(canvas: Canvas?){
        val paint = Paint()
        paint.apply {
            isAntiAlias = true
            color = colorOfShape
            style = Paint.Style.FILL
        }
        canvas?.drawCircle((x), (y), radius, paint)
    }

    override fun getRad(): Float {
        return radius
    }

}
