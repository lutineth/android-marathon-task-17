package com.example.hw17

import android.graphics.Canvas
import android.graphics.Color
import kotlin.random.Random

open class Shape {
    open var x:Float = 0f
    open var y:Float = 0f
    var id:Int = 0
    var colorOfShape: Int = Color.WHITE
    var type:String = ""
    fun setColor(){
        when (Random.nextInt(1, 6)){
            1->colorOfShape = Color.BLACK
            2->colorOfShape = Color.RED
            3->colorOfShape = Color.YELLOW
            4->colorOfShape = Color.BLUE
            5->colorOfShape = Color.GREEN
            6->colorOfShape = Color.CYAN
        }
    }

    open fun draw(canvas: Canvas?){
    }

    open fun getWidth():Float{
        return 0f
    }

    open fun getHeight():Float{
        return 0f
    }

    open fun getRad():Float{
        return 0f
    }

    open fun setWidth(f:Float){
    }

    open fun setHeight(f:Float){
    }
}
