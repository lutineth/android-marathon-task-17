package com.example.hw17

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val canvas:MyView = findViewById(R.id.canvas)
        val addCircle: FloatingActionButton = findViewById(R.id.addCircle)
        val addRectangle: FloatingActionButton = findViewById(R.id.addRectangle)
        val reset: FloatingActionButton = findViewById(R.id.resetButton)

        reset.setOnClickListener{
            canvas.reset()
        }
        addCircle.setOnClickListener {
            canvas.addCircle()
        }
        addRectangle.setOnClickListener {
            canvas.addRectangle()
        }
    }
}
